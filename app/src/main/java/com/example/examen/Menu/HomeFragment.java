package com.example.examen.Menu;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.examen.R;
import com.example.examen.common.AddRoom;
import com.example.examen.common.ChangeRoom;
import com.example.examen.common.Map;

public class HomeFragment extends Fragment {

    public HomeFragment() {
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ImageButton btnAdd = view.findViewById(R.id.btnAdd);
        LinearLayout lvMap = view.findViewById(R.id.lvMaps);
        LinearLayout lvLiving = view.findViewById(R.id.lvLiving);
        LinearLayout lvKitchen = view.findViewById(R.id.lvKitchen);
        LinearLayout lvBathroom = view.findViewById(R.id.lvBathroom);

        TextView tv_name = view.findViewById(R.id.tv_name);
        TextView tv_name2 = view.findViewById(R.id.tv_name2);
        TextView tv_bathroom = view.findViewById(R.id.tv_bathroom);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddRoom.class);
                startActivity(intent);
            }
        });


        lvMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Map.class);
                startActivity(intent);
            }
        });

        lvLiving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeRoom.class);
                intent.putExtra("name", tv_name.getText().toString());
                startActivity(intent);
            }
        });

        lvKitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeRoom.class);
                intent.putExtra("name", tv_name2.getText().toString());
                startActivity(intent);
            }
        });

        lvBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeRoom.class);
                intent.putExtra("name", tv_bathroom.getText().toString());
                startActivity(intent);
            }
        });
        return view;
    }
}