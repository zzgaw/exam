package com.example.examen.common;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.examen.LightFragment;
import com.example.examen.Menu.HomeFragment;
import com.example.examen.R;
import com.example.examen.TerhmostatFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ChangeRoom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_room);

        BottomNavigationView bottom = findViewById(R.id.bottomNavigationView);
        TextView tvName = findViewById(R.id.tvNameOfRoom);
        Intent intent = getIntent();

        String name = intent.getStringExtra("name");
        tvName.setText(name);

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangeRoom.this, MainActivity.class);
                startActivity(intent);
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerView2, new LightFragment()).commit();
        bottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.lightFragment:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerView2, new LightFragment()).commit();
                        return true;
                    case R.id.thermostatFragment:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerView2, new TerhmostatFragment()).commit();
                        return true;
                }
                return false;
            }
        });

    }
}