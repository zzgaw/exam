package com.example.examen.common;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.examen.Menu.HomeFragment;
import com.example.examen.Menu.RoutinesFragment;
import com.example.examen.Menu.SettingsFragment;
import com.example.examen.Menu.StaticsFragment;
import com.example.examen.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNav);
        NavController navController = Navigation.findNavController(this, R.id.flFragment);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }
}